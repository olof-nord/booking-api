package info.olof.booking.controller

import info.olof.booking.service.BookingService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/hello")
class BookingController(private val bookingService: BookingService) {

  @GetMapping
  fun hello() = bookingService.getHello()
}
