package info.olof.booking.controller

import com.ninjasquad.springmockk.MockkBean
import info.olof.booking.service.BookingService
import io.mockk.every
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest

@WebMvcTest
class BookingControllerTests(@Autowired val controller: BookingController) {

  @MockkBean
  private lateinit var bookingService: BookingService

  @Test
  fun `should return by delegating to the booking service`() {
    every { bookingService.getHello() } returns "Hello there"

    assertThat(controller.hello()).isEqualTo("Hello there")
    verify { bookingService.getHello() }
  }


}
