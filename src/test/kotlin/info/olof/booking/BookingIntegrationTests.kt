package info.olof.booking

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.http.HttpStatus

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookingIntegrationTests(@Autowired val restTemplate: TestRestTemplate) {

  @Test
  fun `Assert endpoint content and status code`() {
    val result = restTemplate.getForEntity<String>("/hello")

    assertThat(result.statusCode).isEqualTo(HttpStatus.OK)
    assertThat(result.body).contains("Hello World")

  }

}
